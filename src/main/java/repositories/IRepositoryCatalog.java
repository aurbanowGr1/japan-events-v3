package repositories;

import domain.Categories;
import domain.Events;
import domain.Groups;
import domain.Users;

public interface IRepositoryCatalog {
    public IRepository<Groups> getGroups();
    public IRepository<Categories> getCategories();
    public IRepository<Users> getUsers();
    public IRepository<Events> getEvents();
}
