package repositories.Implementations;

import domain.Categories;
import domain.Events;
import domain.Groups;
import domain.Users;
import repositories.IRepository;
import repositories.IRepositoryCatalog;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@Stateless
public class RepositoryCatalog implements IRepositoryCatalog {

    @Inject
    private EntityManager em;

    @Override
    public IRepository<Groups> getGroups() {
        return new JPA_GroupRepository(em);
    }

    @Override
    public IRepository<Categories> getCategories() {
        return null;
    }

    @Override
    public IRepository<Users> getUsers() {
        return new JPA_UserRepository(em);
    }

    @Override
    public IRepository<Events> getEvents() {
        return new JPA_EventRepository(em);
    }
}

