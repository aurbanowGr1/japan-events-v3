package repositories.Implementations;

import domain.Groups;
import repositories.IRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class JPA_GroupRepository implements IRepository<Groups> {

    private EntityManager em;

    public JPA_GroupRepository(EntityManager em) {
        super();
        this.em = em;
    }

    @Override
    public Groups get(int id) {
        return em.find(Groups.class, id);
    }

    @Override
    public Groups getByName(String name) {
        return em.find(Groups.class, name);
    }

    @Override
    public List<Groups> getAll() {
        return em.createNamedQuery("groups.all", Groups.class).getResultList();
    }

    @Override
    public void add(Groups groups) {
        em.persist(groups);
    }

    @Override
    public void delete(Groups groups) {
        em.remove(groups);
    }

    @Override
    public void update(Groups groups) {

    }
}
