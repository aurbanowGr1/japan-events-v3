package repositories.Implementations;

import domain.Events;
import repositories.IRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class JPA_EventRepository implements IRepository<Events> {

    private EntityManager em;

    public JPA_EventRepository(EntityManager em) {
        super();
        this.em = em;
    }

    @Override
    public Events get(int id) {
        return em.find(Events.class, id);
    }

    @Override
    public Events getByName(String name) {
        return null;
    }

    @Override
    public List<Events> getAll() {
        return em.createNamedQuery("events.all", Events.class).getResultList();
    }

    @Override
    public void add(Events events) {
        em.persist(events);
    }

    @Override
    public void delete(Events events) {
        em.remove(events);
    }

    @Override
    public void update(Events events) {

    }
}
