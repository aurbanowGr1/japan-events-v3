package repositories.Implementations;

import domain.Users;
import repositories.IRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class JPA_UserRepository implements IRepository<Users> {

    private EntityManager em;

    public JPA_UserRepository(EntityManager em) {
        super();
        this.em = em;
    }

    @Override
    public Users get(int id) {
        return em.find(Users.class, id);
    }

    @Override
    public Users getByName(String name) {
        return em.find(Users.class, name);
    }

    @Override
    public List<Users> getAll() {
        return em.createNamedQuery("users.all", Users.class).getResultList();
    }

    @Override
    public void add(Users users) {
        em.persist(users);
    }

    @Override
    public void delete(Users users) {
        em.remove(users);
    }

    @Override
    public void update(Users users) {
    }
}
