package web;

import domain.Users;
import repositories.IRepositoryCatalog;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@SessionScoped
@Transactional
@Named("userBean")
public class UserBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private ListDataModel<Users> users = new ListDataModel<Users>();
    private boolean loggedIn;
    @Inject
    private IRepositoryCatalog catalog;
    private Users user = new Users();

/*  @ManagedProperty(value = "#{param.login}")
    private String login;*/
    private Users uByName = new Users();

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public ListDataModel<Users> getUsers() {
        users.setWrappedData(catalog.getUsers().getAll());
        return users;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String saveUser()
    {
        catalog.getUsers().add(user);
        user = new Users();
        return "home";
    }

    public String log()
    {
        List<Users> users = catalog.getUsers().getAll();

        for(Users u : users)
        {
            if(u.getLogin().equals(user.getLogin())
                    && u.getPassword().equals(user.getPassword()))
            {
                saveToSession();
            }
        }

        return "home?faces-redirect=true";
    }

    private void saveToSession()
    {
        FacesContext fc = FacesContext.getCurrentInstance();

        HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);

        if(user.getLogin() != null && !user.getLogin().equals(""))
        {

            session.setAttribute("user", user);
        }
    }

    public String logout()
    {
        deleteFromSession();
        return "home?faces-redirect=true";
    }

    public void deleteFromSession() {
        FacesContext fc = FacesContext.getCurrentInstance();

        HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);

        if(user.getLogin() != null && !user.getLogin().equals(""))
        {
            session.removeAttribute("user");
        }
    }

    public String deleteUser() {
        catalog.getUsers().delete(this.user);
        deleteFromSession();

        return "home";

    }

    public String AdeleteUser() {

        Users user2 = users.getRowData();
        Users userToDelete = catalog.getUsers().get(user2.getId());

        catalog.getUsers().delete(userToDelete);


        if(user2.getLogin().equals(user.getLogin()))
            deleteFromSession();

        return "showUsers?faces-redirect=true";

    }
}
