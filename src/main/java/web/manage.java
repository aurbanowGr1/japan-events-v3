package web;

import domain.Users;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.Serializable;

@RequestScoped
@Transactional
@ManagedBean(name="other")
public class manage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    UserBean UB;
    private ListDataModel<Users> users = new ListDataModel<Users>();
    @Inject
    private IRepositoryCatalog catalog;
    @ManagedProperty(value=("#{param.id}"))
    private int id;

    private Users user = new Users();


    @PostConstruct
    public void init() {
        user = catalog.getUsers().get(id);
    }

    public Users getUser() {
        return user;
    }


    public void setUser(Users user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String deleteUser() {
        catalog.getUsers().delete(user);
        UB.deleteFromSession();

        return "home";
    }
}
