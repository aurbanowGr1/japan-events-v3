package web;

import domain.Groups;
import domain.Users;
import repositories.IRepositoryCatalog;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@SessionScoped
@Transactional
@Named("groupBean")
public class GroupBean implements Serializable {

    private static final long serialVersionUID = 1L;
    String adminpassword = "projekcik";
    @Inject
    UserBean user = new UserBean();
    private ListDataModel<Groups> groups = new ListDataModel<Groups>();
    @Inject
    private IRepositoryCatalog catalog;
    private Groups group = new Groups();

    public ListDataModel<Groups> getGroups() {
        groups.setWrappedData(catalog.getGroups().getAll());
        return groups;
    }

    public Groups getGroup() {
        return group;
    }

    public void setGroup(Groups group) {
        this.group = group;
    }

    public String saveGroup()
    {
        if(group.getName()=="Administrator") {
            group.setPassword(adminpassword);
        }

        catalog.getGroups().add(group);
        group = new Groups();
        return "showGroups?faces-redirect=true";
    }

    public String AdeleteGroup() {

        Groups group2 = groups.getRowData();
        Groups groupToDelete = catalog.getGroups().get(group2.getId());

        catalog.getGroups().delete(groupToDelete);

        return "showGroups?faces-redirect=true";

    }

    public String joinGroup() {
        if(group.getName() == "Administrator") {
            if (group.getPassword() == adminpassword) {
                user.getUser().setGroupID(group);
            }
        }

        return "joinGroup?faces-redirect=true";
    }
}
