package web;

import domain.Events;
import repositories.IRepositoryCatalog;

import javax.faces.bean.ManagedProperty;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;

@SessionScoped
@Transactional
@Named("eventBean")
public class EventBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private ListDataModel<Events> events = new ListDataModel<Events>();
    @Inject
    private IRepositoryCatalog catalog;
    @Inject
    private UserBean user;
    private Events event = new Events();

    public ListDataModel<Events> getEvents() {
        events.setWrappedData(catalog.getEvents().getAll());
        return events;
    }

    public void setEvents(ListDataModel<Events> events) {
        this.events = events;
    }

    public Events getEvent() {
        return event;
    }

    public void setEvent(Events event) {
        this.event = event;
    }

    public void getAuthorPhoto() {

    }

    public String saveEvent()
    {
        event.setAuthor(user.getUser().getLogin());
        catalog.getEvents().add(event);
        event = new Events();
        return "home";
    }
}
