package addons;

/**
 * Created by Pariston_ on 2014-12-16.
 */
public class br_Addon {
    public static String replace(String word) {
        word = word.replaceAll("\\r?\\n", "<br />");
        word = word.replace("  ", " &emsp;");
        return word;
    }
}
