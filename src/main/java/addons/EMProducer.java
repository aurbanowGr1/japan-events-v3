package addons;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Pariston_ on 2014-12-17.
 */

public class EMProducer {
    @PersistenceContext(name="connect")
    @Produces
    private EntityManager em;
}
