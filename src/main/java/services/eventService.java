package services;

import domain.Events;
import repositories.IRepositoryCatalog;
import services.dto.EventSummaryDto;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("events")
public class eventService {

    @Inject
    IRepositoryCatalog catalog;

    @POST
    @Path("/add")
    @Consumes("application/json")
    @Transactional
    public String saveEvent(EventSummaryDto event)
    {
        Events e = new Events();
        e.setTitle(event.getTitle());
        e.setDescription(event.getDescription());
        catalog.getEvents().add(e);
        return "OK";
    }

}
