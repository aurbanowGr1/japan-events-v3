package services;

import domain.Users;
import repositories.IRepositoryCatalog;
import services.dto.ShowUsersResponseDto;
import services.dto.UserSummaryDto;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;

@Path("users")
public class userService {

    @Inject
    IRepositoryCatalog catalog;

    @GET
    @Path("/getUsers")
    @Produces("application/json")
    @Transactional
    public ShowUsersResponseDto getUsers()
    {
        ShowUsersResponseDto result = new ShowUsersResponseDto();
        for(Users u : catalog.getUsers().getAll())
        {
            UserSummaryDto user = new UserSummaryDto();
            user.setLogin(u.getLogin());
            user.setPassword(u.getPassword());
            result.getUsers().add(user);
        }
        return result;
    }

    @POST
    @Path("/add")
    @Consumes("application/json")
    @Transactional
    public String saveUser(UserSummaryDto user)
    {
        Users u = new Users();
        u.setLogin(user.getLogin());
        u.setPassword(user.getPassword());
        catalog.getUsers().add(u);
        return "OK";
    }

    @POST
    @Path("/delete")
    @Consumes("application/json")
    @Transactional
    public String deleteUser(UserSummaryDto user)
    {
        Users u = new Users();
        u.setLogin(user.getLogin());
        u.setPassword(user.getPassword());
        catalog.getUsers().delete(u);
        return "OK";
    }

    @POST
    @Path("/log")
    @Consumes("application/json")
    @Transactional
    public String signIn(UserSummaryDto user) {
        //tu nic nie ma, choć powstaaanie
        return "OK";
    }
}
