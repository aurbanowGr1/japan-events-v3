package services.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class ShowUsersResponseDto {
    private List<UserSummaryDto> users = new ArrayList<UserSummaryDto>();

    public List<UserSummaryDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserSummaryDto> users) {
        this.users = users;
    }
}
