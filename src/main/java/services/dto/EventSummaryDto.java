package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EventSummaryDto {
    String title;
    String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
