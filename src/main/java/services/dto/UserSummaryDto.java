package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserSummaryDto {
    String login;
    String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
