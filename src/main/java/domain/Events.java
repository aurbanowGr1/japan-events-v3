package domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQuery(name="events.all", query="SELECT e FROM Events e ORDER BY e.id desc")
public class Events extends EntityBase {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected int id;

    private String title;
    private String description;

    private String author;

    @Temporal( TemporalType.DATE )
    private Date dateday;

    @Temporal( TemporalType.DATE )
    private Date hour;
    private String category;

    private String city;
    private String street;
    private String streetnumber;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetnumber() {
        return streetnumber;
    }

    public void setStreetnumber(String streetnumber) {
        this.streetnumber = streetnumber;
    }

    public Date getHour() {
        return hour;
    }

    public void setHour(Date hour) {
        this.hour = hour;
    }

    public Date getDateday() {
        return dateday;
    }

    public void setDateday(Date dateday) {
        this.dateday = dateday;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
