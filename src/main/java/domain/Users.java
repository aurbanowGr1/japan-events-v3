package domain;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

@Entity
@NamedQuery(name="users.all", query="SELECT u FROM Users u")
public class Users extends EntityBase {

    @OneToMany
    public Set<Events> eventsList;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    protected int id;

    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    @Temporal( TemporalType.DATE )
    Date date = new Date();

    private String login;

    private String password;

    @Column(columnDefinition = "varchar(50) default 'default.jpg'")
    private String photo;
    @Temporal( TemporalType.DATE )
    private Date birthday;
    private String register = dateFormat.format(date);
    private String email;

    @ManyToOne
    private Groups groupID;


    public String getPhoto() {
        if(photo==null) {
            photo = "default.jpg";
        }

        return photo;
    }

    public void setPhoto(String photo) {
        if(photo==null) {
            photo = "default.jpg";
        }

        this.photo = photo;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Groups getGroupID() {
        return groupID;
    }

    public void setGroupID(Groups groupID) {
        this.groupID = groupID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Events> getEvents() {
        return eventsList;
    }

    public void setEvents(Set<Events> eventsList) {
        this.eventsList = eventsList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
