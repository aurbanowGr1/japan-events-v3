package domain;


/**
 * Created by Pariston_ on 2014-12-17.
 */
public abstract class EntityBase {

    protected EntityState state;

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }



}
