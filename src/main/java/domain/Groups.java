package domain;

import javax.persistence.*;

@Entity
@NamedQuery(name="groups.all", query="SELECT g FROM Groups g")
public class Groups extends EntityBase {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    protected int id;

    private String password;
    private String name;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}