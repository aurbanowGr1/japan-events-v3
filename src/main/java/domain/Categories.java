package domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@NamedQuery(name="categories.all", query="SELECT c FROM Categories c")
public class Categories extends EntityBase {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    protected int id;

    private String name;

    @Transient
    public Set<Events> events;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Events> getEvents() {
        return events;
    }

    public void setEvents(Set<Events> events) {
        this.events = events;
    }
}
